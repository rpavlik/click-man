#!/usr/bin/env python3
# Copyright 2025, Rylie Pavlik <rylie@ryliepavlik.com>
#
# SPDX-License-Identifier: MIT

"""Have click_man generate its own manpages."""

import sys
import click_man
import click_man.shell
import click_man.core

click_man.core.write_man_pages(
    "click-man",
    click_man.shell.cli,
    version=sys.argv[1],
    target_dir="debian/tmp/manpages",
)
